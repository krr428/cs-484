// Example program
#include <iostream>
#include <string>

using namespace std;

  int next_thread_id () {
    static int next_thread_id = 0;
    next_thread_id %= 8;
    return (next_thread_id++);
  }

int main()
{
  for (int i = 0; i < 50; i++) {
      cout << "Thread id = " << next_thread_id() << endl;
  }
  

}

