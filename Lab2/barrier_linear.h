#pragma once

#include "simulation.h"
#include <iostream>
#include <cstdint>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <pthread.h>

using namespace std;


class LinearBarrier {
public:

  LinearBarrier (uint_fast16_t NUM_THREADS) {
    this->NUM_THREADS = NUM_THREADS;
    this->threads_counted_in = 0;

    pthread_mutex_init(&count_lock, NULL);
    pthread_cond_init(&released, NULL);
  }

  void barrier_wait () {
    pthread_mutex_lock(&count_lock);

    this->threads_counted_in += 1;

    if (this->threads_counted_in == NUM_THREADS) {
      threads_counted_in = 0;
      pthread_cond_broadcast(&released);
    }
    else {
      while (pthread_cond_wait(&released, &count_lock) != 0);
    }

    pthread_mutex_unlock(&count_lock);
  }

private:
  pthread_mutex_t count_lock;
  pthread_cond_t released;
  uint_fast16_t threads_counted_in;
  uint_fast16_t NUM_THREADS;
};

void barrier_linear_wait(void* barrier) {
  // THIS COULD SEGFAULT IF YOU DON'T SET THINGS UP RIGHT.
  LinearBarrier* lbarrier = (LinearBarrier*)barrier;
  lbarrier->barrier_wait();
}
