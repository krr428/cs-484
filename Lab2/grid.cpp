#include "grid.h"
#include "simulation.h"
#include <cstdint>
#include <iostream>
#include <cstdio>


using namespace std;


void print_grid(const t_grid &grid) {
    for (unsigned int i = 0; i < GRID_SIZE; ++i) {
        for (unsigned int j = 0; j < GRID_SIZE; ++j) {
            printf("%d ", (int) grid[i][j]);
        }
        printf("\n");
    }
    cout.flush();
}

void init_grid(t_grid &grid) {
  grid.clear();
  grid.resize(GRID_SIZE, vector<float>(GRID_SIZE));

  #pragma omp parallel for
  for (uint_fast16_t i = 1; i < GRID_SIZE - 1; ++i) {
    grid[i].clear();
    grid[i].resize(GRID_SIZE, 50.0f);
    grid[i][0] = 0.0f;
    grid[i][GRID_SIZE - 1] = 0.0f;
  }

  // Set the top row to 0 degrees.
  grid[0].resize(GRID_SIZE, 0.0f);

  // Set the bottom row to 100 degrees.
  grid[GRID_SIZE - 1].resize(GRID_SIZE, 100.0f);

  fix_grid(grid);
}


bool is_cell_special(const t_grid& grid, const uint_fast16_t row, const uint_fast16_t col) {
  return grid[row][col] == 100.0f || grid[row][col] == 0.0f;
}

void fix_grid(t_grid &grid) {
  // Traditional (Lab 1 grid fix)
  for (uint_fast16_t i = 0; i <= 330; ++i) {
    grid[400][i] = 100.0f;
  }
  grid[200][500] = 100.0f; // This is the last cell to reach steady state

  // Set every twentieth row to be 100.0f degrees
  for (uint_fast16_t row = 0; row < GRID_SIZE; row += 20) {
    // http://stackoverflow.com/questions/8848575
    std::fill(grid[row].begin(), grid[row].end(), 100.0f);
  }

  // Set every twentieth column to be 0.0f degrees
  // TODO: This is stinky and slow; we may want to revisit this later to optimize if possible.
  for (uint_fast16_t row = 0; row < GRID_SIZE; ++row) {
    for (uint_fast16_t col = 0; col < GRID_SIZE; col += 20) {
      grid[row][col] = 0.0f;
    }
  }

}
