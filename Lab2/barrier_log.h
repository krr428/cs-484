#pragma once

#include "simulation.h"
#include <iostream>
#include <cstdint>
#include <mutex>
#include <condition_variable>
#include <atomic>
#include <pthread.h>
#include <vector>

using namespace std;


struct LogBarrierLevel {
  pthread_mutex_t count_lock;
  pthread_cond_t released_up;
  pthread_cond_t released_down;
  uint_fast16_t threads_counted_in;

  LogBarrierLevel () {
    threads_counted_in = 0;
    pthread_mutex_init(&count_lock, NULL);
    pthread_cond_init(&released_up, NULL);
    pthread_cond_init(&released_down, NULL);
  }
};



class LogBarrier {
public:

  LogBarrier (uint_fast16_t NUM_THREADS) {
    this->NUM_THREADS = NUM_THREADS;
    for (uint_fast16_t i = 0; i < NUM_THREADS; i++) {
      levels.push_back(LogBarrierLevel());
    }
  }

  uint_fast16_t next_thread_id () {
    unique_lock<mutex> lck(mtx);
    static uint_fast16_t next_thread_id = 0;
    next_thread_id %= NUM_THREADS;
    cout << next_thread_id << endl;
    uint_fast16_t result = next_thread_id;
    next_thread_id += 1;

    return result;
  }

  void barrier_wait () {

    if (NUM_THREADS == 1) {
      return; // No sense in going through the surgery below.
    }

    int i = 2;
    int base = 0;
    int my_id = next_thread_id();
    int k;

    do {

      k = base + my_id / i;

      if (my_id % i == 0) {

        pthread_mutex_lock(&(levels[k].count_lock));
        levels[k].threads_counted_in++;
        while (levels[k].threads_counted_in < 2) {
          pthread_cond_wait(&(levels[k].released_up), &(levels[k].count_lock));
        }
        pthread_mutex_unlock(&(levels[k].count_lock));

      }
      else {
        pthread_mutex_lock(&(levels[k].count_lock));
        levels[k].threads_counted_in += 1;

        if (levels[k].threads_counted_in == 2) {
          pthread_cond_signal(&(levels[k].released_up));
        }
        while (pthread_cond_wait(&(levels[k].released_down),
                                    &(levels[k].count_lock)) != 0);
        pthread_mutex_unlock(&(levels[k].count_lock));
        break;
      }

      base += NUM_THREADS / i;
      i *= 2;
    } while (i <= NUM_THREADS);

    for (i /= 2; i > 1; i /= 2) {

      base = base - NUM_THREADS / i;
      k = base + my_id / i;

      pthread_mutex_lock(&(levels[k].count_lock));
      levels[k].threads_counted_in = 0;
      pthread_cond_signal(&(levels[k].released_down));
      pthread_mutex_unlock(&(levels[k].count_lock));
    }
  }

private:
  vector<LogBarrierLevel> levels;
  uint_fast16_t NUM_THREADS;
  mutex mtx;
};

void barrier_log_wait(void* barrier) {
  // THIS COULD SEGFAULT IF YOU DON'T SET THINGS UP RIGHT.
  LogBarrier* lbarrier = (LogBarrier*)barrier;
  lbarrier->barrier_wait();
}
