#include <pthread.h>
#include <thread>
#include <vector>
#include <cstdint>
#include <cstdlib>
#include <iostream>
#include <sys/time.h>
#include "barriers.h"
#include "simulation.h"


using namespace std;


typedef void* (*thread_fn_t)(void*);


void run_threads(thread_fn_t threadFunction, t_round_params& templateParams, const uint_fast16_t NUM_THREADS) {

  vector<thread> thread_pool;

  for (uint_fast16_t i = 0; i < NUM_THREADS; i++) {
    t_round_params* params = new t_round_params {
      templateParams.old_grid,
      templateParams.new_grid,
      i,
      NUM_THREADS,
      templateParams.BARRIER_WAIT,
      templateParams.BARRIER
    };

    thread_pool.push_back(std::thread(threadFunction, params));
  }

  for (auto& t : thread_pool) {
    t.join();
  }
}


void pthread_barrier_wait_wrapper(void* barrier) {
  pthread_barrier_wait((pthread_barrier_t*)barrier);
}

double when() {
  struct timeval tp;
  gettimeofday(&tp, NULL);
  return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}


int main (int argc, char* argv[]) {

  double begin = when();

  uint_fast16_t NUM_THREADS = argc > 1 ? atoi(argv[1]) : 1;

  cout << "Running with " << NUM_THREADS << " threads." << endl;

  t_grid A;
  t_grid B;

  init_grid(A);
  init_grid(B);

  // pthread_barrier_t barrier;
  // pthread_barrier_init(&barrier, NULL, NUM_THREADS);

  // SpinlockBarrier barrier(NUM_THREADS);
  // LinearBarrier barrier(NUM_THREADS);
  LogBarrier barrier(NUM_THREADS);

  // t_round_params templateParams {A, B, 0, 0, &pthread_barrier_wait_wrapper, &barrier};
  // t_round_params templateParams {A, B, 0, 0, &barrier_spinlock_wait, &barrier};
  // t_round_params templateParams {A, B, 0, 0, &barrier_linear_wait, &barrier};
  t_round_params templateParams {A, B, 0, 0, &barrier_log_wait, &barrier};

  run_threads(&run_until_steady, templateParams, NUM_THREADS);
  cout << "Total time: " << when() - begin << endl;

  // pthread_barrier_destroy(&barrier);
}
