#pragma once

#include "simulation.h"
#include <cstdint>
#include <random>
#include <atomic>
#include <thread>
#include <mutex>
#include <iostream>
#include <pthread.h>
#include <unordered_map>

using namespace std;


class SpinlockBarrier {
public:

  SpinlockBarrier (uint_fast16_t NUM_THREADS) {
    this->NUM_THREADS = NUM_THREADS;
    this->num_threads_trapped = 0;
    // this->released = false;
  }

  void barrier_wait () {

    mtx.lock();
    threads_trapped[this_thread::get_id()] = true;
    num_threads_trapped++;

    if (num_threads_trapped >= NUM_THREADS) {
      for (auto& kvpair : threads_trapped) {
        threads_trapped[kvpair.first] = false;
      }
      num_threads_trapped = 0;
    }
    mtx.unlock();


    while (true) {
      if (mtx.try_lock()) {
        if (threads_trapped[this_thread::get_id()]) {
          mtx.unlock();
          continue;
        } else {
          mtx.unlock();
          return;
        }
      }
    }


  }

private:
  uint_fast16_t NUM_THREADS;
  mutex mtx;
  unordered_map<thread::id, bool> threads_trapped;
  atomic<uint_fast16_t> num_threads_trapped;
};

void barrier_spinlock_wait(void* barrier) {
  // THIS COULD SEGFAULT IF YOU DON'T SET THINGS UP RIGHT.
  SpinlockBarrier* sbarrier = (SpinlockBarrier*)barrier;
  sbarrier->barrier_wait();
}
