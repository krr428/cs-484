#pragma once

#include <cstdint>
#include "grid.h"


typedef void(*fn_barrier_wait)(void*);

struct t_round_params {

  // t_round_params(
  //   t_grid& old_grid,
  //   t_grid& new_grid,
  //   const uint_fast16_t THREAD_ID,
  //   const uint_fast16_t NUM_THREADS,
  //   fn_barrier_wait BARRIER_WAIT,
  //   void* BARRIER
  // ) {
  //   this->old_grid = old_grid;
  //   this->new_grid = new_grid;
  //   this->THREAD_ID = THREAD_ID;
  //   this->
  // }

  t_grid& old_grid;
  t_grid& new_grid;

  const uint_fast16_t THREAD_ID;
  const uint_fast16_t NUM_THREADS;

  fn_barrier_wait BARRIER_WAIT;
  void* BARRIER;

};

void* run_until_steady(void* argsPointer);
void print_round(t_round_params& params);
void swap_grids(t_round_params& params);
void print_number_over_x(t_round_params& params, float x);
bool is_cell_steady_state(t_grid& grid, uint_fast16_t row, uint_fast16_t col);
bool is_newgrid_steady_state(t_round_params& params);
// void parallel_fix_grid(t_round_params& params);
void calc_temp_for_grid(t_round_params& params);
float calc_temp_for(const t_grid& old_grid, const uint_fast16_t row, const uint_fast16_t col);
