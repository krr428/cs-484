#include "grid.h"
#include <vector>
#include <cstdint>
#include <numeric>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>
#include <iostream>
#include <cmath>
#include "simulation.h"


using namespace std;


void* run_until_steady(void* argsPointer) {
  t_round_params params = *((t_round_params*)argsPointer);

  do {
    print_round(params);
    swap_grids(params);
    calc_temp_for_grid(params);

    // parallel_fix_grid(params);
    // print_number_over_x(params, 50.0f);
  } while (! is_newgrid_steady_state(params));

  //
  // if (params.THREAD_ID == 0) {
  //   print_grid(params.new_grid);
  // }

  return NULL;
}


void print_round(t_round_params& params) {
  if (params.THREAD_ID == 0) {
    static uint_fast16_t round = 0;
    cout << "Beginning round " << ++round << endl;
  }

  params.BARRIER_WAIT(params.BARRIER);
}


void swap_grids(t_round_params& params) {
  if (params.THREAD_ID == 0) {
    std::swap(params.new_grid, params.old_grid);
  }
  params.BARRIER_WAIT(params.BARRIER);
}


// void print_number_over_x(t_round_params& params, float x) {
//
//   static atomic<uint_fast64_t> grand_total;
//
//   if (params.THREAD_ID == 0) {
//     // Initialize space for the subsums.
//     grand_total = 0;
//   }
//
//   params.BARRIER_WAIT(params.BARRIER);
//
//   const uint_fast32_t START_ROW = ((GRID_SIZE * params.THREAD_ID) / (params.NUM_THREADS)) + 1;
//   const uint_fast32_t END_ROW =
//     (params.THREAD_ID != params.NUM_THREADS - 1) ?
//     1 + ((GRID_SIZE * (params.THREAD_ID + 1)) / (params.NUM_THREADS)) : GRID_SIZE - 1;
//
//   for (uint_fast32_t row = START_ROW; row < END_ROW; row += 1) {
//     for (uint_fast32_t col = 1; col < GRID_SIZE - 1; ++col) {
//       if (params.new_grid[row][col] > x && params.new_grid[row][col] < 100.0f) {
//         grand_total++;
//       }
//     }
//   }
//
//
//   params.BARRIER_WAIT(params.BARRIER);
//
//   if (params.THREAD_ID == 0) {
//    cout << "Number of cells > " << x << " is: " << grand_total << endl;
//   }
//
//   params.BARRIER_WAIT(params.BARRIER);
//
// }


bool is_cell_steady_state(t_grid& grid, const uint_fast16_t row, const uint_fast16_t col) {
  if (is_cell_special(grid, row, col)) {
    return true;
  }

  float mine = grid[row][col];
  float avg = (grid[row - 1][col] + grid[row + 1][col] + grid[row][col - 1] + grid[row][col + 1]) * 0.25f;

  return abs(mine - avg) < 0.1f;
}


bool is_newgrid_steady_state(t_round_params& params) {

  static bool result_is_steady_state;

  if (params.THREAD_ID == 0) {

    result_is_steady_state = true;

    /*
      We MUST not let anyone past this point to ensure that the
      result_is_steady_state is initialized exactly once, and
      that this initialization happens _before_ the search.
    */
    static uint_fast16_t last_row = 1;
    static uint_fast16_t last_col = 1;


    // Calculate whether we are in a steady state.
    // const uint_fast16_t INITIAL_ROW = params.THREAD_ID + 1;
    // const uint_fast16_t SKIP_ROWS = params.NUM_THREADS;
    if (! is_cell_steady_state(params.new_grid, last_row, last_col)) {
      result_is_steady_state = false;
    }
    else {
      for (uint_fast16_t row = 1; row < GRID_SIZE - 1 && result_is_steady_state; ++row) {
        for (uint_fast16_t col = 1; col < GRID_SIZE - 1; ++col) {
          if (! is_cell_steady_state(params.new_grid, row, col)) {

            last_row = row;
            last_col = col;

            /* By setting this value, the other threads will get notified that
               we are not in steady state, and that we should bail out */
            result_is_steady_state = false;
            break;
          }
        }
      }
    }

  }
  params.BARRIER_WAIT(params.BARRIER);

  return result_is_steady_state;  // We do not assume this is true here!
}


// void parallel_fix_grid(t_round_params& params) {
//
//   if (params.THREAD_ID == 0) {
//     for (uint_fast16_t i = 0; i <= 330; ++i) {
//       params.new_grid[400][i] = 100.0f;
//     }
//     params.new_grid[200][500] = 100.0f; // This is the last cell to reach steady state
//   }
//
//   params.BARRIER_WAIT(params.BARRIER);
//
//   const uint_fast16_t INITIAL_ROW = 20 * params.THREAD_ID;
//   const uint_fast16_t SKIP_ROWS = 20 * params.NUM_THREADS;
//
//   // Set every twentieth row to be 100.0f degrees
//   for (uint_fast16_t row = INITIAL_ROW; row < GRID_SIZE; row += SKIP_ROWS) {
//     std::fill(params.new_grid[row].begin(), params.new_grid[row].end(), 100.0f);
//   }
//
//   params.BARRIER_WAIT(params.BARRIER);
//
//   // Set every twentieth column to be 0.0f degrees
//   // TODO: This is stinky and slow; we may want to revisit this later to optimize if possible.
//   for (uint_fast16_t row = INITIAL_ROW; row < GRID_SIZE; row++) {
//     for (uint_fast16_t col = 0; col < GRID_SIZE ; col += 20) {
//       params.new_grid[row][col] = 0.0f;
//     }
//   }
//
//   params.BARRIER_WAIT(params.BARRIER);
//
// }


void calc_temp_for_grid(t_round_params& params) {

  static vector<uint_fast32_t> subtotals;

  if (params.THREAD_ID == 0) {
    subtotals.clear();
    subtotals.resize(params.NUM_THREADS, 0);
  }

  params.BARRIER_WAIT(params.BARRIER);

  const uint_fast32_t START_ROW = ((GRID_SIZE * params.THREAD_ID) / (params.NUM_THREADS)) + 1;
  const uint_fast32_t END_ROW =
    (params.THREAD_ID != params.NUM_THREADS - 1) ?
    1 + ((GRID_SIZE * (params.THREAD_ID + 1)) / (params.NUM_THREADS)) : GRID_SIZE - 1;

  for (uint_fast32_t row = START_ROW; row < END_ROW; row += 1) {
    for (uint_fast32_t col = 1; col < GRID_SIZE - 1; ++col) {
      if (! is_cell_special(params.old_grid, row, col)) {
        params.new_grid[row][col] = calc_temp_for(params.old_grid, row, col);
        if (params.new_grid[row][col] > 50) {
          subtotals[params.THREAD_ID]++;
        }
      }
    }
  }

  params.BARRIER_WAIT(params.BARRIER);

  if (params.THREAD_ID == 0) {
    printf("Number over 50: %d\n", accumulate(subtotals.begin(), subtotals.end(), 0));
  }
}

//
inline float calc_temp_for(const t_grid& old_grid, const uint_fast16_t row, const uint_fast16_t col) {
  return (
    old_grid[row - 1][col] +
    old_grid[row][col - 1] +
    old_grid[row + 1][col] +
    old_grid[row][col + 1] +
    (4 * old_grid[row][col])
  ) * 0.125f;
}
