#pragma once

#include <vector>
#include <cstdint>

const uint_fast16_t GRID_SIZE = 16384;

typedef std::vector<std::vector<float> > t_grid;

void print_grid(const t_grid &grid);
void init_grid(t_grid &grid);
bool is_cell_special(const t_grid& grid, const uint_fast16_t row, const uint_fast16_t col);
void fix_grid(t_grid &grid);
