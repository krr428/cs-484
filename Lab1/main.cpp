#include <iostream>
#include <cstdio>
#include <string.h>
#include <vector>
#include <cmath>
#include <sys/time.h>

using namespace std;


const int GRID_SIZE = 16384;

typedef vector<vector<float> > t_grid;

// static t_grid G1;
// static t_grid G2;


// Prints the grid out to the console.

void print_grid(const t_grid &grid) {
    for (unsigned int i = 0; i < GRID_SIZE; ++i) {
        for (unsigned int j = 0; j < GRID_SIZE; ++j) {
            printf("%d ", (int) grid[i][j]);
        }
        printf("\n");
    }
    cout.flush();
}

void clean_grid(t_grid &grid) {
    grid.resize(GRID_SIZE, vector<float>(GRID_SIZE));
    // #pragma openmp parallel for
    for (unsigned int i = 0; i < GRID_SIZE; ++i) {
        grid.at(i).clear();
        grid.at(i).resize(GRID_SIZE, 0);
        // for (unsigned int j = 0; j < GRID_SIZE; ++j) {
        //     grid[i][j] = 0;
        // }
    }
}

float fix_grid_at(const unsigned int row, const unsigned int col, const float current) {
    if (row == GRID_SIZE - 1) {
        return 100.0f;
    }
    else if (row == 0 || col == 0 || col == GRID_SIZE - 1) {
        return 0.0f;
    }
    // else if (row % 20 == 0) {
    //   return 100.0f;
    // }
    // else if (col % 20 == 0) {
    //   return 0.0f;
    // }
    else {
        return current;
    }
}

void init_grid(t_grid &grid) {

    #pragma omp parallel for
    for (unsigned int row = 0; row < GRID_SIZE; ++row) {
        for (unsigned int col = 0; col < GRID_SIZE; ++col) {

            //            float value = (float)sqrt(pow(row - GRID_SIZE / 2.0, 2.0) + pow(col - GRID_SIZE / 2.0, 2.0));
            //            grid[row][col] = 100 - value * (75.0 / GRID_SIZE);

            grid[row][col] = fix_grid_at(row, col, 50);
        }
    }
}

// Returns true if it reaches a steady state.

float inline calc_temp_for(const t_grid &grid, const unsigned int row, const unsigned int col) {
    return ((
            grid[row - 1][col] +
            grid[row][col - 1] +
            grid[row + 1][col] +
            grid[row][col + 1] +
            (4 * grid[row][col])
          ) * 0.125);
}

bool inline is_cell_steady_state(const t_grid &grid, const unsigned int row, const unsigned int col) {

    // if (fix_grid_at(row, col, -50) != -50) {
    //     return true; // Then this is a special square.
    // }

    float mine = grid[row][col];
    float avg = (grid[row - 1][col] + grid[row + 1][col] + grid[row][col - 1] + grid[row][col + 1]) * 0.25;

    return abs(mine - avg) < 0.1;
}

bool inline is_grid_in_steady_state(const t_grid &grid) {
  for (unsigned int row = 1; row < GRID_SIZE - 1; ++row) {
      for (unsigned int col = 1; col < GRID_SIZE - 1; ++col) {
          if (!is_cell_steady_state(grid, row, col)) {
              return false;
          }
      }
  }

  return true;
}

void fix_grid(t_grid &grid) {
  // We make the assumption that the

  // #pragma openmp parallel for
  // for (unsigned int i = 0; i <= 330; ++i) {
  //     grid[400][i] = 100.0f;
  // }
  // grid[200][500] = 100.0f;
}

bool run_round(const t_grid &grid_old, t_grid &grid_new) {

    #pragma omp parallel for
    for (unsigned int row = 1; row < GRID_SIZE - 1; ++row) {
        for (unsigned int col = 1; col < GRID_SIZE - 1; ++col) {
            grid_new[row][col] = calc_temp_for(grid_old, row, col);
        }
    }

    fix_grid(grid_new);

    return is_grid_in_steady_state(grid_new);
}

double when() {
    struct timeval tp;
    gettimeofday(&tp, NULL);
    return ((double) tp.tv_sec + (double) tp.tv_usec * 1e-6);
}

int main() {

    double begin = when();

    t_grid G1;
    t_grid G2;
    clean_grid(G1);
    clean_grid(G2);
    init_grid(G1);
    init_grid(G2);

    unsigned int round;

    for (round = 0; !run_round(G1, G2); ++round) {
            //  cout << "Not yet steady at round " << round << "." << endl;
      //  if (round % 10 == 0) {
      //      cout << "Not yet steady on round " << round << endl;
      //  }
        swap(G1, G2);
    }



    cout << "Steady state found at: " << round << endl;
    // print_grid(*G1);

    // delete G1;
    // delete G2;

    cout << "Total time: " << when() - begin << endl;
}
