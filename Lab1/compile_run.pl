# system(g++ -std=c++11 -O3 -fopenmp main.cpp && ./a.out);

for my $x (1, 2, 4, 8, 16, 24) {
	system("export OMP_NUM_THREADS=$x && ./a.out > output-$x.txt");
}
